import Vue from 'vue'
import VueRouter from 'vue-router'

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.config.productionTip = false

import Home from './pages/Home.vue'
import Form1 from './pages/Form-1.vue'

const routes = [
  { path: '/', component: Home},
  { path: '/form-1', component: Form1},
];

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  router
}).$mount('#app')
